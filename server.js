var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');
var dbConf = require('./dbConf');
var port = process.env.PORT || 8080;

//routes external
var apiRouter = require('../rest/routes/api');


app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// configure our app to handle CORS requests
app.use(function(req, res, next){
	//Configuracion para que desde cualquier dominio
	//puedan consumir la api
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Authorization');
	next();
});

//log request
app.use(morgan('dev'));

//routes for api
app.get('/', function(req, res){
	res.send('Bienvenido a la pagina principal');
});

//Registro de las Rutas
app.use('/api', apiRouter);

//inicio del server
app.listen(port);
console.log('Run server on port: ' + port);