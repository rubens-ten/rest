//instancia de express router
var express = require('express');
var apiRouter = express.Router();

//User model
var User = require('../../rest/models/user');

//middleware for all request with api router
apiRouter.use(function (req, res, next){
	console.log('Alguien se conecto a la aplicacion REST API');
	next();
});

//Acceso a /api
apiRouter.get('/', function(req, res){
	res.json({message : 'hello, welcome to api'});
});

//Controlador para las peticiones sobre usuarios
apiRouter.route('/users')

//creacion de un usuario por medio de post
.post(function(req, res){
	//instancia nueva de usuario
	var user = new User();
	//seteo de la informacion de usuario
	user.name = req.body.name;
	user.username = req.body.username;
	user.password = req.body.password;

	//Se guarda el usuario en la bd
	user.save(function(err){
		if(err) {
			//se analiza si hubo un entrada duplicada
			if(err.code == 11000)
				return res.json({sucess: false, message: 'Un usuario con este nombre de usuario ya existe.'});
			else
				return res.send(err);
		}

		res.json({message : 'Usuario creado'});

	});
})
.get(function(req, res){
	User.find(function(err, users){
		if(err) res.send(err);

		//Retorna los usuarios
		res.json(users);
	});
})


module.exports = apiRouter;
