var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');

//Esquema de usuario
var UserSchema = new Schema({
	name : String,
	username : {
		type: String, required : true,
		index : {unique: true}
	},
	password : {
		type: String,
		required: true,
		select: false
	}
});

//hash password antes de guardar el usuario
UserSchema.pre('save', function(next){
	var user = this;
	//hash de el pass  si el pass ha cambiado o el usuario es nuevo
	if(!user.isModified('password'))return next();

	//generacion del hash
	bcrypt.hash(user.password, null, null, function(err, hash){
		if(err) return next();

		//cambio del pass a la version hash
		user.password = hash;
		next();
	});
});


UserSchema.methods.comparePassword = function(){
	var user = this;
	return bcrypt.compareSync(password, user.password);
};

module.exports = mongoose.model('User', UserSchema);